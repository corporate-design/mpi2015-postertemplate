#!/usr/bin/env texlua

module = "mpi2015-poster"
sourcefiles = {"*"}
installfiles = {"*.cls", "Logos"}
bstfiles = {"*.bst"}
