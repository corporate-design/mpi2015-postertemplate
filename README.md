# MPI2015 Poster Template

LaTeX class following the MPI 2015 corporate design rules for
poster creation.

## Installation


First clone this repository locally,

```bash
# clone using SSH (you may also use HTTPS link instead)
git clone git@gitlab.mpi-magdeburg.mpg.de:corporate-design/mpi2015-postertemplate.git
```

Install the files in a path that is recursively searched by latex e.g. this can be your texmf folder.
Otherwise the logos will not be found.

To install this template in your texmf, simply run from the project directory,

```bash
l3build install
```

> The included comfort for TikZ users seems to require tikZ/PGF 3.00 or higher for proper nesting of elements without exceeding the TeX capacities.
> TeXlive 2014 has been reported to be sufficient to get the `example.tex` compiled.


## Usage Instructions

See the `example/` directory for a sample poster explaining the usage.


## Notes

The background color in the (content) node is slightly off of the value given in the Handbook.
This is due to the fact, that it was wrong in all powerpoint templates and thus it was easier to fix it centrally by changing the color to the wrong value here in order to make the posters look alike.
